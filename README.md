# KMS Server (vlmcsd) (KMS激活服务器 Docker版)

>## **1. Dockerfile**
> **ilemonrain/vlmcsd:latest**： [Dockerfile](https://hub.docker.com/r/ilemonrain/vlmcsd/~/dockerfile/)
>
>## **2. 镜像介绍**
> 此镜像，基于 [Alpine Latest](https://hub.docker.com/_/alpine/) 构建，程序部分基于 [vlmcsd](https://github.com/Wind4/vlmcsd.git) 编译，镜像大小仅4.2MB，是杀人放火（划去）居家旅行必备轻量级KMS服务端！
>
>## **3. 食用说明**
> 命令格式：
> 
> **docker run -d ilemonrain/vlmcsd**
>
> 不需要额外挂任何参数！默认会自动绑定1688端口（KMS默认就是这个端口），如果用于公有云Docker，直接使用最低配置启动即可，无需挂任何参数！
> 
> 如果万不得已，必须改端口，使用以下命令：
>
> **docker run -d -p 2688:1688 ilemonrain/vlmcsd**
>
>## **4. 调戏作者（划去）BUG反馈与交流**
>Email 到 ilemonrain@ilemonrain.com